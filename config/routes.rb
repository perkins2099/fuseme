FuseMe::Application.routes.draw do

  #Rails Admin
  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'

  #Task Resources
  resources :tasks, :only => [:show, :destroy, :create, :update, :new, :edit]
  match '/sync_facebook' => 'integrations#sync_facebook', :as => 'sync_facebook'
  
  #Profile Resources
  resources :profiles, :only => [:update, :edit]
  match '/me' => 'profiles#me', :as => 'me'
  #match '/profiles/:id/edit' => 'profiles#edit', :as => 'edit_profile'
#  match '/profiles/:id' => 'profiles#full_contact_profile', :as => 'full_contact_profile'

  #Connection Resources
  resources :connections, :only => [:destroy, :create]
  match '/contacts' => 'connections#index', :as => 'contacts'
  
  #User System Resources
  devise_for :users
  match '/settings' => 'users#settings', :as => 'settings'

  #Integration Resources
  resources :integrations, :only => [:new,:destroy,:create, :index]
  match 'auth/:provider/callback' => 'integrations#create'
  match '/dashboard' => 'tasks#dashboard', :as => 'dashboard'
  match '/sync_all' => 'integrations#sync_all', :as => 'sync_all'
  
  
  #Default
  root :to => "tasks#dashboard"

  #Lowest priority
  #match '/:user_name' => 'profiles#full_contact_profile', :as => 'full_contact_profile'
  match '/profiles/:id' => 'profiles#full_contact_profile', :as => 'full_contact_profile'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
