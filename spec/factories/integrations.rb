# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :integration do
    name "facebook"
    uid "1234567890"
    last_sync 148732438
    user_id 1
  end
  
end
