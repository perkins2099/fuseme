# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
  	email "test@test.com"
  	password "testtest"
  	admin false
  	stub false
  end
end
