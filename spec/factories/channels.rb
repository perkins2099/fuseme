# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :channel do
    user_id "1"
    channel_type "facebook"
    value "1234567890"
  end
end
