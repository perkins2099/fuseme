require 'spec_helper'

describe IntegrationsHelper do
	describe "functions" do
		it "should return a link and a string" do
			@i = FactoryGirl.create(:integration, provider:'facebook')
			url_provider(@i).should_not be_nil
		end
	end
end
