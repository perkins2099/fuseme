require "spec_helper"

describe ProfilesController do
  describe "routing" do

    it "routes to #edit" do
      get("/profiles/1/edit").should route_to("profiles#edit", :id => "1")
    end

    it "routes to #me" do
      post("/me").should route_to("profiles#me")
    end

 #   it "routes to #full_contact_profile" do
 #     profile = Profile.new(user_id: 1, user_name: "cakebatter99875")
 #     profile.save
 #     get("/cakebatter99875").should route_to("profiles#full_contact_profile")
 #   end


  end
end
