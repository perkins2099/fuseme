require "spec_helper"

describe ConnectionsController do
  describe "routing" do

    it "routes to #index" do
      get("/contacts").should route_to("connections#index")
    end

    it "routes to #create" do
      post("/connections").should route_to("connections#create")
    end

    it "routes to #destroy" do
      delete("/connections/1").should route_to("connections#destroy", :id => "1")
    end

  end
end
