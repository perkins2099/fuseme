require "spec_helper"

describe IntegrationsController do
  describe "routing" do

    it "routes to #sync_all" do
      get("/sync_all").should route_to("integrations#sync_all")
    end
    it "routes to #sync_facebook" do
      get("/sync_facebook").should route_to("integrations#sync_facebook")
    end

     it "routes to #create" do
       post("/integrations").should route_to("integrations#create")
     end

     it "routes to #destroy" do
       delete("/integrations/1").should route_to("integrations#destroy", :id => "1")
     end

     it "routes to #index" do
       get("/integrations").should route_to("integrations#index")
     end
     it "routes to #create 2" do
       post("/auth/:provider/callback").should route_to("integrations#create", :provider => ":provider")
     end
     it "routes to #new" do
      get("/integrations/new").should route_to("integrations#new")
     end
  end
end
