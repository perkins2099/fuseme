require 'spec_helper'


describe User do
  	before do
   		@user = User.new(email: "user@example.com", password: "3sdioooo", password_confirmation:"3sdioooo", created_at: Time.now, updated_at: Time.now) 
  	end

	subject { @user }
	
	describe "when email is not present" do
      before { @user.email = " " }
      it { should_not be_valid }
  	end

	describe "when email address is already taken" do
		before do
			user_with_same_email = @user.dup
			user_with_same_email.save
	 	end
	 	it { should_not be_valid }
	end

	describe "attributes" do
		it { should respond_to(:email) }
		it { should respond_to(:password) }
		it { should respond_to(:password_confirmation) }
		it { should respond_to(:remember_me) }
		it { should respond_to(:created_at) }
		it { should respond_to(:updated_at) }
	end

	describe "create_profile" do
		it "should create a new profile after a user gets created" do
			@user.save!
			@user.profile.nil?.should be_false
		end
	end

	describe "associations" do
		it "should respond to received_tasks" do
			@user = FactoryGirl.create(:user, email: "receiver@test.com")
			@sender = FactoryGirl.create(:user, email: "sender@test.com")
			@task = FactoryGirl.create(:task, user_id:@user.id, sender_id:@sender.id)		
			@user.received_tasks.first.should eq @task
		end

		it "should respond to sent_tasks" do
			@user = FactoryGirl.create(:user, email: "receiver@test.com")
			@sender = FactoryGirl.create(:user, email: "sender@test.com")
			@task = FactoryGirl.create(:task, user_id:@sender.id, sender_id:@user.id)	
			@user.sent_tasks.first.should eq @task
		end
		it "should respond to profile" do
			@user = FactoryGirl.create(:user)
			#@task = FactoryGirl.create(:task, user_id:@user.id, sender_id:2)	
			@user.profile.should_not be_nil
		end
		it "should respond to contacts" do
			@user = FactoryGirl.create(:user)
			@user2 = FactoryGirl.create(:user, :email =>"asdfoij@asdfoij.com")
			@connection = FactoryGirl.create(:connection, :user_id =>@user.id, :contact_id =>@user2.id)
			@user.contacts.first.should eq @user2
		end
	end

	describe "functions" do
		it "locate_stub_user should locate a user" do
			
			@stub_user = FactoryGirl.create(:user, :stub => true, email: "test@noway.com")
			@user = FactoryGirl.create(:user, :stub => false, email: "sender@fuseme.com")
			@connection = FactoryGirl.create(:connection, user_id: @user.id, contact_id: @stub_user.id) 
			@channel = FactoryGirl.create(:channel, user_id: @stub_user.id, 
				channel_type: "facebook", value: "1234567890")

			@user.locate_stub_user("facebook", "1234567890").should eq(@stub_user)

		end
	end

end




