require 'spec_helper'

describe Profile do
  before do
  	@profile = FactoryGirl.create(:profile)
  end

	subject { @profile }

	describe "attributes" do
		it { should respond_to(:first_name) }
		it { should respond_to(:last_name) }
		it { should respond_to(:user_name) }
		it { should respond_to(:user_id) }
		it { should respond_to(:image) }
	end

	describe "Associations" do
  	it "belongs to a user" do  		
  		@user = FactoryGirl.create(:user)
  		@profile.user_id = @user.id
			@profile.user.should eq(@user)			
  	end
  end

	describe "Callbacks" do
		it "should downcase the user_name when storing" do
  		@profile.user_name = "DiddyDog"
  		@profile.save

  		@profile.user_name.downcase.should be_true
		end
	end

	describe "Validations" do
		it "should not be valid if user_name is missing" do
			should validate_presence_of :user_name
		end  
		it "should not be valid if user_name has more than letter and numbers" do
			@profile.user_name = "8*78f8%"
			@profile.save.should be_false
		end 
		it "should not be valid if user_name has reserved names" do
			@profile.user_name = "contact"
			@profile.save.should be_false
		end 
		it "should not be valid if user_name is to long" do
			@profile.user_name = "contact35g57835363338683753535353yusgfuihdhjsfguih4t879gryugdhj"
			@profile.save.should be_false
		end 
		it "should not be valid if user_name is not unique" do
			@profile.user_name = "coji"
			@profile.save
			@profile2 = @profile
			@profile2.id = nil
			@profile2.user_name = 'coJi'
			#debugger
			@profile2.save.should be_false
		end 
		it "should not be valid if user_name is to short" do
			@profile.user_name = "con"
			@profile.save.should be_false
		end 
	end

	describe "Functions" do
		it "should return first_name + last_name" do
			@profile.full_name.should eq (@profile.first_name + ' ' + @profile.last_name)
		end
	end

end
