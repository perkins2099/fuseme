require 'spec_helper'

describe TaskGroup do
	describe "attributes" do
		it "should respond to ID" do
			@taskgroup= TaskGroup.new(id:1)
			@taskgroup.should respond_to(:id)
		end
	end


	describe "associations" do
		it "a task_group should have tasks" do
			@taskgroup= TaskGroup.new(id:1)
			@task1 = FactoryGirl.create(:task, task_group_id:@taskgroup.id)		
			@task2 = FactoryGirl.create(:task, task_group_id:@taskgroup.id)		
			@task3 = FactoryGirl.create(:task, task_group_id:@taskgroup.id)				
			@taskgroup.tasks.count.should eq(3)
		end
	end

end
