require 'spec_helper'


describe Task do

	subject { @task }

	describe "attributes" do
	  before do
  		@task = FactoryGirl.create(:task)
  	end

		it { should respond_to(:title) }
		it { should respond_to(:body) }
		it { should respond_to(:location) }
		it { should respond_to(:start_time) }
		it { should respond_to(:end_time) }
		it { should respond_to(:task_type) }
		it { should respond_to(:system) }
		it { should respond_to(:user_id) }
		it { should respond_to(:sent) }
		it { should respond_to(:read) }
		it { should respond_to(:read_time) }
		it { should respond_to(:sent_time) }
		it { should respond_to(:status) }
		it { should respond_to(:task_group_id) }
		it { should respond_to(:sender_id) }
	end

	describe "associations" do
		it "a task should respond to receiver" do
			@user = FactoryGirl.create(:user, email: "receiver@test.com")
			@sender = FactoryGirl.create(:user, email: "sender@test.com")
			@task = FactoryGirl.create(:task, user_id:@user.id, sender_id:@sender.id)		
			@task.receiver.should eq @user
		end
		it "a task should respond to sender" do
			@user = FactoryGirl.create(:user, email: "receiver@test.com")
			@sender = FactoryGirl.create(:user, email: "sender@test.com")
			@task = FactoryGirl.create(:task, user_id:@user.id, sender_id:@sender.id)		
			@task.sender.should eq @sender
		end
		it "a task should respond to task_group" do
			@user = FactoryGirl.create(:user, email: "receiver@test.com")
			@sender = FactoryGirl.create(:user, email: "sender@test.com")
			@task_group = FactoryGirl.create(:task_group)
			@task = FactoryGirl.create(:task, user_id:@user.id, sender_id:@sender.id, task_group_id: @task_group.id)		
			@task2 = FactoryGirl.create(:task, user_id:@user.id, sender_id:@sender.id, task_group_id: @task_group.id)		
			@task3 = FactoryGirl.create(:task, user_id:@user.id, sender_id:@sender.id, task_group_id: @task_group.id)		
			@task.task_group.tasks.count.should eq(3)
		end
	end

	describe "Validations" do
		it "should not be valid if user_id is nil" do
			@task = FactoryGirl.create(:task)		
			@task.should validate_presence_of :user_id
		end  
		it "should not be valid if sender_id is nil" do
			@task = FactoryGirl.create(:task)		
			@task.should validate_presence_of :sender_id
		end 
		it "should not be valid if title is nil" do
			@task = FactoryGirl.create(:task)		
			@task.should validate_presence_of :title
		end
	end

 	describe "Functions" do
	 	it "should return all messages between a user and sender" do
	 		@user = FactoryGirl.create(:user, email: "receiver@test.com")
	 		@sender = FactoryGirl.create(:user, email: "sender@test.com")
	 		@task = FactoryGirl.create(:task, user_id:@user.id, sender_id:@sender.id)		
	 		@task2 = FactoryGirl.create(:task, user_id:@sender.id, sender_id:@user.id)		
	 		@task3 = FactoryGirl.create(:task, user_id:@user.id, sender_id:@sender.id)		

	 		@all_tasks = Task.dialog(@user.id,@sender.id)
	 		@all_tasks.count.should eq(3)
	 	end
		it "read - should set a task to read and save the time" do
			@user = FactoryGirl.create(:user, email: "receiver@test.com")
			@task = FactoryGirl.create(:task, read: false, read_time: nil, user_id:@user.id)			
			@task.read!
			@task.read.should be_true
		end
	end
end