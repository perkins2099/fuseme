require 'spec_helper'

describe Connection do
	before do
  	@con = FactoryGirl.create(:connection)
  end

	subject { @con }

	describe "attributes" do
		it { should respond_to(:contact_id) }
		it { should respond_to(:user_id) }
	end


  describe "Associations" do
  	it "belongs to a user" do
  		@user = FactoryGirl.create(:user, email: "user@test.com")
			@contact = FactoryGirl.create(:user, email: "contact@test.com")
  		@connection = FactoryGirl.create(:connection, user_id: @user.id, contact_id: @contact.id)
			@user.contacts.first.should eq(@contact)			
  	end
  	 it "user has connections" do
  		@user = FactoryGirl.create(:user, email: "user@test.com")
			@contact = FactoryGirl.create(:user, email: "contact@test.com")
  		@connection = FactoryGirl.create(:connection, user_id: @user.id, contact_id: @contact.id)
			@user.connections.count.should eq(1)
  	end
	end

	describe "Callbacks" do
		it "should not let connection save when assigning connection to self" do
			@user = FactoryGirl.create(:user, email: "user@test.com")
  		#@connection = FactoryGirl.create(:connection, user_id: @user.id, contact_id: @user.id)
  		@connection = Connection.new(user_id: @user.id, contact_id: @user.id)
  		@connection.save.should be_false
		end
		it "should not let connection save when assigning a dupliate" do		
  		@connection = Connection.new(user_id: 1, contact_id: 2)
  		@connection2 = Connection.new(user_id: 1, contact_id: 2)
  		@connection.save
  		@connection2.save.should be_false
		end
	end

	describe "Validations" do
		it "should not be valid if user_id is nil" do
			should validate_presence_of :user_id
		end  
		it "should not be valid if contact_id is nil" do
			should validate_presence_of :contact_id
		end 
	end



end
