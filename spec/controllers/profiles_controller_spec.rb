require 'spec_helper'

describe ProfilesController do

  describe "Must be Signed In" do
    it "calls me while not Signed In" do
      get :me
      response.should redirect_to (new_user_session_path)     
    end
#    it "calls full_contact_profile while not Signed In" do
#      get :full_contact_profile #, {:scene => valid_attributes}
#      response.should redirect_to (new_user_session_path)     
#    end
    #  it "calls create while not Signed In" do
    #   post :create #, {:scene => valid_attributes}
    #   response.should redirect_to (new_user_session_path)     
    # end
     # it "calls new while not Signed In" do
     #  get :new
     #  response.should redirect_to (new_user_session_path)     
   # end
    # it "calls edit while not Signed In" do     
    #   get :edit
    #   response.should redirect_to (new_user_session_path)     
    # end
    # it "calls update while not Signed In" do
    #   put :update#, {:id => scene.to_param, :scene => valid_attributes}#, valid_session
    #   response.should redirect_to (new_user_session_path)     
    # end
  end

  describe "GET me" do
    login_user
    it "@profile should not be nil" do   
      get :me
      assigns[:profile].should_not be_nil
    end
  end
  describe "GET full_contact_profile" do
    login_user
    it "should find a user through param[:user_name]" do   
      @profile = FactoryGirl.create(:profile, user_id: subject.current_user.id)
      get :full_contact_profile, user_name: subject.current_user.profile.user_name
      assigns[:profile].should_not be_nil
    end
    it "should return all tasks between current_user and whoevers profile" do   
      @profile = FactoryGirl.create(:profile, user_id: subject.current_user.id)
      get :full_contact_profile, user_name: subject.current_user.profile.user_name
      assigns[:tasks].should_not be_nil
    end
  end


end
