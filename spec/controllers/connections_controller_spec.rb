require 'spec_helper'


describe ConnectionsController do

  describe "Must be Signed In" do
    it "calls Index while not Signed In" do
      get :index
      response.should redirect_to (new_user_session_path)     
    end
    it "calls create while not Signed In" do
      post :create #, {:scene => valid_attributes}
      response.should redirect_to (new_user_session_path)     
    end
    # it "calls delete while not Signed In" do
    #   delete :destroy# , {:id => scene.to_param}#, valid_session
    #   response.should redirect_to (new_user_session_path)     
    # end
  end

end
