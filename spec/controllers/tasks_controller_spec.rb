require 'spec_helper'

describe TasksController do

  describe "Must be Signed In" do
	  it "calls dashboard while not Signed In" do
	    get :dashboard
	    response.should redirect_to (new_user_session_path)     
	  end
	  # it "calls show while not Signed In" do
	  #   get :show
	  #   response.should redirect_to (new_user_session_path)     
	  # end

	  it "calls create while not Signed In" do
	    post :create #, {:scene => valid_attributes}
	    response.should redirect_to (new_user_session_path)     
	  end
	  # it "calls delete while not Signed In" do
	  #   delete :destroy# , {:id => scene.to_param}#, valid_session
	  #   response.should redirect_to (new_user_session_path)     
   #  end
    it "calls new while not Signed In" do
      get :new
      response.should redirect_to (new_user_session_path)     
    end
    # it "calls edit while not Signed In" do
    #   get :edit
    #   response.should redirect_to (new_user_session_path)     
    # end
    # it "calls update while not Signed In" do
    #   put :update#, {:id => scene.to_param, :scene => valid_attributes}#, valid_session
    #   response.should redirect_to (new_user_session_path)     
    # end
  end

  describe "GET new" do
    login_user
    it "if passing user_id should set @task.user_id" do   
    	@user2 = FactoryGirl.create(:user, email: "test@ttt.com")
    	@connection = FactoryGirl.create(:connection,user_id: subject.current_user.id, contact_id:@user2.id)  	
    	get :new, user_id: @user2.id
      assigns[:task].user_id.should_not be_nil
    end
    it "if passing task_id should set @task.user_id" do   
    	@user2 = FactoryGirl.create(:user, email: "test@ttt.com")
    	@connection = FactoryGirl.create(:connection,user_id: subject.current_user.id, contact_id:@user2.id)  	
    	@task = FactoryGirl.create(:task, user_id:subject.current_user.id, sender_id: @user2.id)
    	get :new, task: @task.id
      assigns[:task].user_id.should_not be_nil
    end
  end
  
end
