module ApplicationHelper

	def display_to_value(user_id)
		user = User.find(user_id)
		return user.email
	end

	def value_to_display(email)
		user = User.find_by_email(email)
			return user.id
	end

	def display_name(user_id)
		if user_id.nil? then return "" end
		user = User.find(user_id)		
		str = user.profile.first_name + ' ' + user.profile.last_name
		#if not read
		#	str = "<strong>" + str + "</strong>"
		#end
		return str
	end

	def display_stepped_datetime(t)
			if t.nil? then
				return '01/01/1990'
			end
			t = Time.at(t)
		if t.strftime("%m/%d/%Y") == Time.now.strftime("%m/%d/%Y") then t.strftime("%I:%M%p") else t.strftime("%m/%d/%Y") end 
	end

end
