class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  #Associations
    has_many :sent_tasks, :class_name => 'Task'    ,:foreign_key => 'sender_id', :inverse_of => :sender
    has_many :received_tasks, :class_name => 'Task',:foreign_key => 'user_id', :inverse_of => :receiver

    has_many :connections, :dependent => :destroy
    has_many :contacts, :through => :connections, :dependent => :destroy

    has_many :inverse_connections, :class_name => "Connection", :foreign_key => "contact_id", :dependent => :destroy
    has_many :inverse_contacts, :through => :inverse_connections, :source => :user, :dependent => :destroy

    has_one :profile, :inverse_of => :user, :dependent => :destroy

    has_many :integrations, :inverse_of => :user, :dependent => :destroy

    has_many :channels, :inverse_of => :user, :dependent => :destroy

  #Attributes
    attr_accessible :email, :password, :password_confirmation, :remember_me, :created_at, :updated_at, :stub
  
  #Callbacks
    after_create :create_profile

  #Other
    mount_uploader :image, ImageUploader
  
  #Scopes
    #default_scope :joins => :profile
    scope :stubs, where(:stub => true)
 
  def create_profile  #When a user is created make sure a profile is created for them as well
    #debugger
    profile = Profile.new
    profile.user_id = id
    profile.user_name = "newuser" + (id+12000).to_s()
    profile.save
  end


  def locate_stub_user(provider, uid)
    puts "my id:" + id.to_s
    puts "provider:" + provider.to_s
    puts "uid:" + uid.to_s


    channel = Channel.where(:user_id => Connection.select('contact_id').where(:user_id => id), 
      :value => uid.to_s).first
    
    if channel.nil?
      return nil
    else
      user = User.find_by_id(channel.user_id)
    end
    
    if user.stub then
      return user
    else
      return nil
    end
  end
end
