class Channel < ActiveRecord::Base
  #Associations
  	belongs_to :user#, :inverse_of => :channels
    #belongs_to :way, :class_name => "User"

  #Attributes
	  attr_accessible :channel_type, :user_id, :value

	#Validation
  	validates :user_id, :presence => true
  	validates :channel_type, :presence => true
  	validates :value, :presence => true
end
