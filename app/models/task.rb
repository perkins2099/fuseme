class Task < ActiveRecord::Base
	attr_accessible :title, :body, :location, :start_time, :end_time,
		:task_type, :system, 
		:user_id, :sent, :read, :read_time, :sent_time, :status,
		:status_time, :task_group_id, :sender_id, :previous_system_id, :transmitted_at
	
	#Associations
	belongs_to :receiver, :class_name => 'User', :foreign_key => 'user_id', :inverse_of => :received_tasks
	belongs_to :sender, :class_name => 'User', :foreign_key => 'sender_id', :inverse_of => :sent_tasks
	belongs_to :task_group, :inverse_of => :tasks

	#Callbacks
	after_save { |task| task.transmitted_at = Time.now.to_i }

 	#Validations
	validates :user_id, :presence => true
	validates :sender_id, :presence => true
	validates :title, :presence => true

	#Scopes
	scope :default_task_order, :order => ("tasks.transmitted_at desc" )

	#Get the dialog between the sender and receiver
	def self.dialog(user_id, sender_id)
		where("(user_id = ? and sender_id = ?) or (user_id = ? and sender_id = ?)" , user_id, sender_id, sender_id, user_id)
	end

	def read!
		self.read = true
		self.read_time = Time.now
		self.save
	end

end

