class Profile < ActiveRecord::Base
  #Associations
  	belongs_to :user, :inverse_of => :profile

  #Attributes
  	attr_accessible :first_name, :image, :last_name, :user_id, :user_name, :remote_image_url

  #Callbacks
    before_save { |profile| profile.user_name = profile.user_name.downcase }

  #Scopes
    scope :default_profile_order, order(:first_name).order(:last_name)

  #Other
	mount_uploader :image, ImageUploader

  #Validation
  validates :user_name, :presence => true
  validates :user_name, :format => { :with => /^\w+$/i,
    :message => "only letters and numbers allowed" }
  validates :user_name, :exclusion => {:in => %w(contact dashboard profile profiles ass dick cunt prick shit fuck connection contacts connections home), :message => "Please choose a different user name"} 
  validates :user_name, :length => {:maximum => 50, :too_long => "%{count} characters is the maximum allowed"}
  validates :user_name, :length => {:minimum => 4, :too_short => "%{count} characters is the manimum allowed"}
  validates :user_name, :uniqueness => { :case_sensitive => false }

	def full_name
      "#{self.first_name} #{self.last_name}"
  end

  def full_name=(fullname)
      first,last = fullname.split(' ')  # or some smarter way to split
      self.first_name = first
      self.surname = last
  end

  #When adding a new contact and you have contacts already
  def self.list_profiles_to_add(user_id)
    user = User.find(user_id)
    where(user_id: User.select("id")).where("profiles.user_id not in (?)",user.contacts.select("contact_id").map(&:contact_id)).where("profiles.user_id !=?",user.id)
  end
  #When adding a new contact and you have no contacts
  def self.list_profiles_to_add_when_no_others(user_id)
    user = User.find(user_id)
    where(user_id: User.select("id")).where("profiles.user_id !=?",user.id)
  end

end
