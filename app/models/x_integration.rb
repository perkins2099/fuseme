class XIntegration < ActiveRecord::Base
  attr_accessible :name, :id

  has_many :integrations, :inverse_of => :x_integration
end
