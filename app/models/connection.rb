class Connection < ActiveRecord::Base
  #Callbacks
    before_save :validate_unique_connection
    before_save :validate_cannot_add_self

  #Associations
    belongs_to :user
    belongs_to :contact, :class_name => "User"

  #Attributes
    attr_accessible :contact_id, :user_id


  #Validations
    validates :user_id, :presence => true
    validates :contact_id, :presence => true

  #Scopes
    #default_scope :joins => :profile, :conditions => {:contact_id => :id}

  def validate_unique_connection 	#User can only add a connection to someone they don't have
  	Connection.where(contact_id: self.contact_id, user_id: self.user_id).count == 0
  end

  def validate_cannot_add_self  #User cannot add a connection to themselves
    self.contact_id != self.user_id
  end


end
