class Integration < ActiveRecord::Base
	#Associations
		belongs_to :user, :inverse_of => :integrations
		belongs_to :x_integration, :inverse_of => :integrations

  #Attributes
  	attr_accessible :name, :user_id, :x_integration_id, :status, :uid, :provider, :token, :secret, :oauth_expires_at, :last_sync

 	#Validations
 		validates :user_id, :presence => true
 		#validates :x_integration_id, :presence => true

	def setup(omniauth)	#Standard Fields available - Https://github.com/intridea/omniauth/wiki/Auth-Hash-Schema
		self.name = omniauth[:info][:nickname]
		self.save
	end

	def login_facebook()	#Get the object to log into facebook and return
    Koala::Facebook::API.new(self.token)
	end

	def self.fb_sync_direct_messages(id)
		
		i = find(id)
		current_user = User.find(i.user_id)

		fb = i.login_facebook
		
 		#Time Data
 		#i.last_sync =	'2012-01-01'.to_time.to_i
    sync_time = Time.now.to_i

    threads = fb.fql_query ("SELECT thread_id, recipients, updated_time, message_count, viewer_id FROM thread WHERE folder_id = 0")
    

		#Grab most recent threads since last sync (Received / inbox)
		recent_threads = threads.select {|t| t["updated_time"].to_i > i.last_sync and t["updated_time"].to_i <= sync_time}
		thread_ids = recent_threads.map { |t| t['thread_id']}
		messages = fb.fql_query ("SELECT thread_id, message_id, author_id,body,created_time,attachment FROM message WHERE thread_id in (" + thread_ids.to_s.sub("[","").sub("]","") + ")")
		#Grab most recent messages since last sync where I am NOT the author
		recent_messages = messages.select {|t| t["created_time"].to_i > i.last_sync and 
					t["updated_time"].to_i <= sync_time  and 
					t['author_id'].to_s != i.uid.to_s and
					Task.find_by_previous_system_id_and_system(t['message_id'].to_s, 'Facebook') == nil
				} 
		puts "---------------------------------->Facebook Sync<------------------------------"
		#Debug Information
		puts "Rthreads:--------------->" + recent_threads.count.to_s
		puts "MESSAGES:--------------->" + messages.count.to_s
		puts "RMESSAGES:--------------->" + recent_messages.count.to_s
		puts "Time:---------------->" + Time.at(i.last_sync).to_s
		puts "Now Time:---------------->" + Time.at(sync_time).to_s

		recent_messages.each do |message|	
			puts message['message_id']
		 	user = current_user.locate_stub_user( 'facebook',message['author_id'].to_s ) 
		 	user = Integration.fb_setup_new_user(message['author_id'], fb, i) if user.nil?

			user.destroy if not self.fb_create_task(message, user.id, i.user_id)
		end		

		#-------------------------------->Sent Messages--------------------------------
		
		#Grab most recent threads since last sync (sent / outbox)
		threads = fb.fql_query ("SELECT thread_id, recipients, updated_time, message_count, viewer_id FROM thread WHERE folder_id = 1")		
		#Grab most recent threads since last sync (Received / inbox)
		recent_threads = threads.select {|t| t["updated_time"].to_i > i.last_sync and t["updated_time"].to_i <= sync_time}
		thread_ids = recent_threads.map { |t| t['thread_id']}
		messages = fb.fql_query ("SELECT thread_id, message_id, author_id,body,created_time,attachment FROM message WHERE thread_id in (" + thread_ids.to_s.sub("[","").sub("]","") + ")")
		#Grab most recent messages since last sync where I am the author
		recent_messages = messages.select {|t| t["created_time"].to_i > i.last_sync and 
					t["updated_time"].to_i <= sync_time  and 
					t['author_id'].to_s == i.uid.to_s and
					Task.find_by_previous_system_id_and_system(t['message_id'].to_s, 'Facebook') == nil
				} 

		#puts threads.to_yaml

		#Debug Information
		puts "Rthreads:--------------->" + recent_threads.count.to_s
		puts "MESSAGES:--------------->" + messages.count.to_s
		puts "RMESSAGES:--------------->" + recent_messages.count.to_s
		puts "Time:---------------->" + Time.at(i.last_sync).to_s
		puts "Now Time:---------------->" + Time.at(sync_time).to_s
		#puts recent_threads.to_yaml

		recent_messages.each do |message|	
			puts message['message_id'] + "----->threadID:" + message['thread_id']
			thread =  recent_threads.select {|t| t['thread_id'] == message['thread_id']}
			recipients = thread.first['recipients']
					
			recipients = recipients.delete_if {|m| m.to_s == i.uid.to_s }

			recipients.each do |recipient|
			 	user = current_user.locate_stub_user('facebook',recipient.to_s ) 			 	 	
			 	user = fb_setup_new_user(recipient.to_s, fb,i ) if user.nil?
				user.destroy if not self.fb_create_task(message, i.user_id, user.id)
			end
			
		end	

		i.last_sync = sync_time
		i.save

	end

	def self.fb_create_task(message, sent_id, to_id)
		task = Task.new
		task.transmitted_at = message['created_time']
		task.previous_system_id = message['message_id'].to_s			
		task.sender_id = sent_id
		task.user_id = to_id #self.user_id
		task.title = message['body'].split[0...6].join(' ') + '...'
		task.body = message['body']
		task.system = 'Facebook'
		task.task_type = 'EMail'
		task.save
	end


	#If a stub user needs to be created here are the steps
	def self.fb_setup_new_user(uid, fb, i)				
	 	user = User.create(stub: true, email: uid.to_s + '_' + i.user.id.to_s +  '_seed@fuseme.com' )
 		user.save(:validate => false)
			
		#Update the profile with as much information as possible
		fb_info = fb.fql_query ("select first_name, middle_name, last_name, pic_big, username, pic_small, pic_square,birthday_date, sex, about_me, profile_url, contact_email  from user where uid = '" +  uid.to_s + "'")

		#Connect stub user with actual user
		connection = Connection.new(user_id: i.user_id, contact_id: user.id )
		connection.save!

		#Create a channel where this contact was located from
		channel = Channel.new(user_id: user.id, channel_type: 'facebook', value: uid.to_s )
		channel.save!

		if not fb_info.first.nil?
			user.profile.first_name = fb_info.first['first_name']
			user.profile.last_name = fb_info.first['last_name']		 		
			user.profile.remote_image_url = fb_info.first['pic_big']
			user.profile.save
		else 
			user.profile.first_name = 'Who'
			user.profile.last_name = 'AmI'
			user.profile.save

			#user.destroy
			#channel.destroy
		end

		return user
	end


end
