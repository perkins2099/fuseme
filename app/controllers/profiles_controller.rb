class ProfilesController < ApplicationController
#Rails.logger.info("PARAMS: #{params.inspect}")   
  
  before_filter :require_login

  def require_login
    unless user_signed_in?
      redirect_to new_user_session_path, flash: {:error => "You must be logged in to continue to access this section"}
    end
  end

   def me
    @profile = Profile.find_by_user_id(current_user.id)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @profile }
    end
  end
  
  def full_contact_profile  #Will render the view that a user sees when they view a contact

    #Get the appropriate profile based on the user_name passed
    #@profile = Profile.find_by_user_name(params[:user_name].downcase)
    @profile = Profile.find_by_id(params[:id])

    #Prepare the list of tasks shared between current_user and this profile
    user_id = current_user.id
    sender_id = @profile.user_id
    @tasks = Task.dialog(user_id, sender_id).default_task_order

    if @profile.nil? or Connection.find_by_user_id_and_contact_id(current_user.id, @profile.user_id).nil?
      then
        respond_to do |format|
          format.html { redirect_to '/dashboard', notice: 'You cannot view that page' }
         
        end
    else
        respond_to do |format|
          format.html # show.html.erb
          format.json { render json: @profile }
       end     
    end
 
  end
 
  # GET /profiles/new
  # GET /profiles/new.json
  def new
    @profile = Profile.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @profile }
    end
  end

  # GET /profiles/1/edit
  def edit  
    @profile = Profile.find(params[:id])
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(params[:profile])

    respond_to do |format|
      if @profile.valid? and @profile.save
        format.html { redirect_to '/me', notice: 'Profile was successfully created.' }
        format.json { render json: @profile, status: :created, location: @profile }
      else
        format.html { render action: "new" }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /profiles/1
  # PUT /profiles/1.json
  def update
    @profile = Profile.find(params[:id])

    respond_to do |format|
      if @profile.update_attributes(params[:profile])
        format.html { redirect_to '/me', notice: 'Profile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

end