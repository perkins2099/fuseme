class IntegrationsController < ApplicationController
#include TwitterHelper
#include FacebookHelper

	 before_filter :require_login

  def require_login
    unless user_signed_in?
      redirect_to new_user_session_path, flash: {:error => "You must be logged in to continue to access this section"}
    end
  end

	def index
  	@integrations = current_user.integrations if current_user
  end

  def sync_all
    current_user.integrations.each do |i|
      if i.provider == 'facebook' then
        QC.enqueue "Integration.fb_sync_direct_messages", i.id
      end
    end
  redirect_to '/dashboard', flash: {:notice => "Your messages are being synced"}
  end

  def sync_facebook
    i = current_user.integrations.find_by_provider('facebook')
    QC.enqueue "Integration.fb_sync_direct_messages", i.id
    #Integration.fb_sync_direct_messages(i.id)   
    redirect_to :back, flash: {:notice => "Your FB is being synced"}
  end


  def create
 		omniauth = request.env["omniauth.auth"]
    #puts "----------------->omniauth" + omniauth.to_s()
    integration = Integration.find_by_provider_and_uid(omniauth['provider'], omniauth['uid'])
    if integration and not current_user   #If they authenticated and not currently logged in
      flash[:notice] = "Signed in successfully"
      sign_in_and_redirect(:user,integration.user)
    elsif integration and current_user
      flash[:notice] = "Integration already added"
      redirect_to '/integrations'
    elsif not integration and current_user

     # if omniauth['provider'] = 'twitter' then
      #  puts "-------------------t>" + omniauth['provider']
        integration = current_user.integrations.create!(
          provider: omniauth['provider'], 
          uid: omniauth['uid'],
          token: omniauth['credentials']['token'],
          secret: omniauth['credentials']['secret'],
          last_sync: (Time.now.to_i - (60*60*24*7) ) )
      # elsif omniauth['provider'] ='facebook' then
      #   puts "-------------------fb>" + omniauth['provider']
      #   integration = current_user.integrations.create!(
      #     provider: omniauth['provider'], 
      #     uid: omniauth['uid'],
      #     token: omniauth['credentials']['token'],
      #     secret: omniauth['credentials']['secret'],
      #     oauth_expires_at: Time.at(omniauth.credentials.expires_at) )
      # end


      integration.setup(omniauth)
      flash[:notice] = "Integration successful."
  	  redirect_to '/integrations'
    end
    
  end

 # GET /sis/new
  # GET /sis/new.json
  def new
    @integration = Integration.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @integration }
    end
  end

  def destroy
    @integration = current_user.integrations.find(params[:id])
  	@integration.destroy
  	flash[:notice] = "Successfully destroyed integration."
  	redirect_to :back
  end


end
