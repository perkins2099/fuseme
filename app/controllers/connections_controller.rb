class ConnectionsController < ApplicationController
  include ApplicationHelper

  before_filter :require_login

  def require_login
    unless user_signed_in?
      redirect_to new_user_session_path, flash: {:error => "You must be logged in to continue to access this section"}
    end
  end

  def index
    @connections = current_user.connections
    @connection = Connection.new()

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @connections }
    end
  end

  # DELETE /connections/1
  # DELETE /connections/1.json
  def destroy
    #@connection = Connection.find(params[:id])
    @connection = current_user.connections.find(params[:id])
    @connection.destroy

    respond_to do |format|
      format.html { redirect_to '/contacts', notice: 'The contact was removed'  }
      format.json { head :no_content }
    end
  end

  def create
    #Rails.logger.info("PARAMS: #{params.inspect}")
    contact_ids = params[:connection][:contact_id]
    contact_ids.each do |contact_id|
      @connection = Connection.new(user_id: current_user.id, contact_id: contact_id)
      status = @connection.save
    end

    if status
      redirect_to :back, notice: 'Contacts added'
    else
      redirect_to :back, notice: 'Problem adding contacts.  Not added.'
    end
  end 

end



