#Rails.logger.info("PARAMS: #{params.inspect}")   
class TasksController < ApplicationController
  include ApplicationHelper

  before_filter :require_login


  def require_login
    unless user_signed_in?
      redirect_to new_user_session_path, flash: {:error => "You must be logged in to continue to access this section"}
    end
  end

  def dashboard    
    @tasks = current_user.received_tasks.default_task_order
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tasks }
    end
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
    @show_task = Task.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @task }
    end
  end

  # GET /tasks/new
  # GET /tasks/new.json
  def new
    @task = Task.new

    #Grab params
    @task.user_id = params[:user_id] if not params[:user_id].nil? and current_user.contacts.exists?(params[:user_id])
    task_id = params[:task]    

    #Make sure the task specified to reply to is for one I've sent or receieved
    if task_id != nil then
      if current_user.sent_tasks.exists?(task_id) or current_user.received_tasks.exists?(task_id)
        @old_task = Task.find(task_id)
        @task.user_id = @old_task.sender_id
        @task.task_group_id = @old_task.task_group_id
        @task.title = @old_task.title
      end
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @task }
    end
  end

  # GET /tasks/1/edit
  def edit
    @task = Task.find(params[:id])
  end

  # POST /tasks
  # POST /tasks.json
  def create
    #Rails.logger.info("PARAMS: #{params.inspect}")
    @task = Task.new(params[:task])
    @task.sent_time = Time.now
    @task.read = false
    @task.sender_id = current_user.id
    if @task.task_group_id.nil?
      then
        task_group = TaskGroup.new()
        task_group.save
        @task.task_group_id = task_group.id
    end

    respond_to do |format|
      if @task.save #Save the sending Task
          format.html { redirect_to @task, notice: 'Task was successfully created.' }
          format.json { render json: @task, status: :created, location: @task }
        else
          @task.destroy
          format.html { render action: "new" }
          format.json { render json: @task.errors, status: :unprocessable_entity }
        end
      end
  end

  # PUT /tasks/1
  # PUT /tasks/1.json
  def update
    @task = Task.find(params[:id])

    respond_to do |format|
      if @task.update_attributes(params[:task])
        format.html { redirect_to @task, notice: 'Task was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task = Task.find(params[:id])
    @task.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

end
