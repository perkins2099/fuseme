class UsersController < ApplicationController

	before_filter :require_login

  def require_login
    unless user_signed_in?
      redirect_to new_user_session_path, flash: {:error => "You must be logged in to continue to access this section"}
    end
  end

  def settings
    @integrations = current_user.integrations
    @new_integration = Integration.new

    respond_to do |format|
      format.html # settings
      format.json { render json: @integrations }
    end
  end

end
