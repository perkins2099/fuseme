# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120629024724) do

  create_table "channels", :force => true do |t|
    t.integer  "user_id"
    t.string   "channel_type"
    t.string   "value"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "channels", ["value"], :name => "index_channels_on_value"

  create_table "connections", :force => true do |t|
    t.integer  "user_id"
    t.integer  "contact_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "integrations", :force => true do |t|
    t.integer  "user_id"
    t.integer  "x_integration_id"
    t.string   "uid"
    t.string   "status"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "provider"
    t.string   "name"
    t.string   "token"
    t.string   "secret"
    t.datetime "oauth_expires_at"
    t.integer  "last_sync",        :limit => 8
  end

  add_index "integrations", ["user_id"], :name => "index_integrations_on_user_id"

  create_table "profiles", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "image"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
    t.string   "user_name"
  end

  add_index "profiles", ["user_id"], :name => "index_profiles_on_user_id"
  add_index "profiles", ["user_name"], :name => "index_profiles_on_user_name"

  create_table "rails_admin_histories", :force => true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      :limit => 2
    t.integer  "year",       :limit => 8
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], :name => "index_rails_admin_histories"

  create_table "task_groups", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tasks", :force => true do |t|
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "title"
    t.text     "body"
    t.string   "location"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "user_id"
    t.boolean  "read"
    t.boolean  "sent"
    t.datetime "sent_time"
    t.datetime "read_time"
    t.integer  "task_group_id"
    t.integer  "sender_id"
    t.string   "system"
    t.string   "status"
    t.string   "task_type"
    t.string   "previous_system_id"
    t.integer  "transmitted_at"
  end

  add_index "tasks", ["previous_system_id"], :name => "index_tasks_on_previous_system_id"
  add_index "tasks", ["sender_id"], :name => "index_tasks_on_sender_id"
  add_index "tasks", ["user_id"], :name => "index_tasks_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "admin",                  :default => false
    t.string   "image"
    t.boolean  "stub",                   :default => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "x_integrations", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "x_system_integrations", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
