class RenameColumnInTasks < ActiveRecord::Migration
 	def change
 		remove_column :tasks, :sent_or_received
 	end
end
