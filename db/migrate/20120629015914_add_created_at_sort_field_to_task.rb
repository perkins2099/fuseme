class AddCreatedAtSortFieldToTask < ActiveRecord::Migration
  def change
  	add_column :tasks, :transmitted_at, :integer
  end
end
