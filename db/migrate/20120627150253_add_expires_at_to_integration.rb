class AddExpiresAtToIntegration < ActiveRecord::Migration
  def change
  	add_column :integrations, :oauth_expires_at, :datetime
  end
end
