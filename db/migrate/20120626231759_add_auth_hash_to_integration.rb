class AddAuthHashToIntegration < ActiveRecord::Migration
  def change
  	add_column :integrations, :name, :string
  	add_column :integrations, :token, :string
  	add_column :integrations, :secret, :string
  end
end
