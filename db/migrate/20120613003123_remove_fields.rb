class RemoveFields < ActiveRecord::Migration
 def change
 	remove_column :profiles, :user_id
 	add_column :users, :profile_id, :integer

 	remove_column :users, :first_name
 	remove_column :users, :last_name
 end
end
