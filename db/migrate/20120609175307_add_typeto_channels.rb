class AddTypetoChannels < ActiveRecord::Migration
  def up
  	add_column :channels, :channel_type_id, :integer
  end

  def down
  	remove_column :channels, :channel_type_id
  end
end
