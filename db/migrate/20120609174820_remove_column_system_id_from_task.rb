class RemoveColumnSystemIdFromTask < ActiveRecord::Migration
  def up
  	remove_column :tasks, :system_id
  	
  end

  def down
  	add_column :tasks, :system_id, :integer
  end
end
