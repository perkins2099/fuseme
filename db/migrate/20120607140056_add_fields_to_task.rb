class AddFieldsToTask < ActiveRecord::Migration
  def change
  	add_column :tasks, :title, :string
  	add_column :tasks, :body, :string
  	add_column :tasks, :location, :string

   	add_column :tasks, :start_time, :datetime
   	add_column :tasks, :end_time, :datetime

   	add_column :tasks, :system_id, :integer, :null => false
   	add_column :tasks, :task_type_id, :integer, :null => false

  end
end

