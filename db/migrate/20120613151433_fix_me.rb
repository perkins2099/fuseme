class FixMe < ActiveRecord::Migration
  def change
  	remove_column :users, :profile_id
  	add_column :profiles, :user_id, :integer
  	add_index :profiles, :user_id
  end

end
