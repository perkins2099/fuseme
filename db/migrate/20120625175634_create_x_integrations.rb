class CreateXIntegrations < ActiveRecord::Migration
  def change
    create_table :x_integrations do |t|
      t.string :name

      t.timestamps
    end
  end
end
