class CreateSystemIntegrationTable < ActiveRecord::Migration
  def up
    create_table :system_integrations do |t|
      t.integer :user_id
      t.string :name
      t.timestamps
    end
    add_index :system_integrations, :user_id
  end

  def down
  	drop_table :system_integrations
  end
end
