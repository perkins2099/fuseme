class DropTAble < ActiveRecord::Migration
  def change
  	drop_table :system_integrations

  	create_table :integrations do |t|
      t.integer :user_id
      t.string :name
      t.timestamps
    end
    add_index :integrations, :user_id

  end

end
