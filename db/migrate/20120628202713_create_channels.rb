class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.integer :user_id
      t.string :channel_type
      t.string :value

      t.timestamps
    end

    add_index :channels, :value

  end
end
