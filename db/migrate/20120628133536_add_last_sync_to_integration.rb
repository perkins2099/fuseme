class AddLastSyncToIntegration < ActiveRecord::Migration
  def change
  	add_column :integrations, :last_sync, :bigint
  end
end
