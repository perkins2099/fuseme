class AddPreviousIdToTask < ActiveRecord::Migration
  def change
  	add_column :tasks, :previous_system_id, :string
  	add_index :tasks, :previous_system_id
  end
end
