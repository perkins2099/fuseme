class AddStubToUser < ActiveRecord::Migration
  def change
  	add_column :users, :stub, :boolean, :default => false
  end
end
