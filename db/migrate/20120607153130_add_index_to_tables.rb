class AddIndexToTables < ActiveRecord::Migration
  def change
  	add_index :receivers, :user_id
  	add_index :receivers, :task_id

  	add_index :senders, :user_id
  	add_index :senders, :task_id

    remove_column :tasks, :user_id
    remove_column :tasks, :task_id

  end

end
