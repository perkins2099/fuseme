class RenameFieldsWithNoLookups < ActiveRecord::Migration
  def up
  	remove_column :tasks, :task_type_id
  	remove_column :tasks, :status_id
  	remove_column :tasks, :system_id

  	add_column :tasks, :system, :string
  	add_column :tasks, :status, :string
  	add_column :tasks, :type, :string

  end

  def down
  end
end
