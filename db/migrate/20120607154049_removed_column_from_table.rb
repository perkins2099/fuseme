class RemovedColumnFromTable < ActiveRecord::Migration
  def up
  	remove_column :tasks, :sent
  	remove_column :tasks, :sent_time
  end

  def down
  end
end
