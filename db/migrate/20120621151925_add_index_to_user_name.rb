class AddIndexToUserName < ActiveRecord::Migration
  def change
  	add_index :profiles, :user_name
  end
end
