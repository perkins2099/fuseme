class SimplifyMessenging < ActiveRecord::Migration
  def up
  	drop_table :channels

  	add_column :tasks, :user_id, :integer
  	add_column :tasks, :status_id, :integer
  	add_column :tasks, :sent_or_received, :integer
  	add_column :tasks, :system_id, :integer

  	add_column :tasks, :read, :boolean
  	add_column :tasks, :sent, :boolean

  	add_column :tasks, :sent_time, :datetime
  	add_column :tasks, :read_time, :datetime

  	add_index :tasks, :user_id
  	add_index :tasks, :system_id


  end

  def down
  	 create_table "channels", :force => true do |t|
    t.integer  "user_id"
    t.integer  "task_id"
    t.integer  "status"
    t.integer  "system_id"
    t.datetime "status_time"
    t.boolean  "read"
    t.datetime "read_time"
    t.string   "sent"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.datetime "sent_time"
    t.integer  "channel_type_id"

     add_index "channels", ["task_id"], :name => "index_channels_on_task_id"
  	add_index "channels", ["user_id"], :name => "index_channels_on_user_id"
  end

  remove_column :tasks, :user_id
  remove_column :tasks, :status_id
  remove_column :tasks, :sent_or_received
  remove_column :tasks, :system_id
  remove_column :tasks, :task_type_id
  remove_column :tasks, :read
  remove_column :tasks, :sent
  remove_column :tasks, :sent_time
  remove_column :tasks, :read_time

  end
end
