class ChangeTaskBodyToHoldMoreData < ActiveRecord::Migration
  def change
  	change_column :tasks, :body, :text
  end
end
