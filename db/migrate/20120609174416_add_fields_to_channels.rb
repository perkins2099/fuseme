class AddFieldsToChannels < ActiveRecord::Migration
  def change
  	add_column :channels, :sent_time, :datetime
  	add_index :channels, :user_id
  	add_index :channels, :task_id

  end
end
