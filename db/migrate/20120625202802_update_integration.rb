class UpdateIntegration < ActiveRecord::Migration
  def up
  	add_column :integrations, :provider, :string
  end

  def down
  end
end
