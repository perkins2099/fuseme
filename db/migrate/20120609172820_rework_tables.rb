class ReworkTables < ActiveRecord::Migration
  def up
  	drop_table :senders
  	drop_table :receivers

  end

  def down

  create_table "receivers", :force => true do |t|
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "user_id",                           :null => false
    t.integer  "task_id",                           :null => false
    t.boolean  "read",           :default => false
    t.boolean  "completed",      :default => false
    t.datetime "opened_time"
    t.datetime "read_time"
    t.datetime "replied_time"
    t.datetime "completed_time"
  end

  add_index "receivers", ["task_id"], :name => "index_receivers_on_task_id"
  add_index "receivers", ["user_id"], :name => "index_receivers_on_user_id"

  create_table "senders", :force => true do |t|
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "user_id",                       :null => false
    t.integer  "task_id",                       :null => false
    t.boolean  "sent",       :default => false
    t.datetime "sent_time"
  end

  add_index "senders", ["task_id"], :name => "index_senders_on_task_id"
  add_index "senders", ["user_id"], :name => "index_senders_on_user_id"

  end
end
