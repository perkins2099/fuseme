class AddSenderIdToTask < ActiveRecord::Migration
  def change
  	add_column :tasks, :sender_id, :integer
  	add_index :tasks, :sender_id
  end
end
