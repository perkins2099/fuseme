class AddFieldsToTables < ActiveRecord::Migration
  def change
  		  add_column :senders, :user_id, :integer, :null => false
	  add_column :senders, :task_id, :integer, :null => false

	  add_column :senders, :sent, :boolean, :default => false

	  add_column :senders, :sent_time, :datetime

	  
		add_column :receivers, :user_id, :integer, :null => false
		add_column :receivers, :task_id, :integer, :null => false

		add_column :receivers, :read, :boolean, :default => false
		add_column :receivers, :completed, :boolean, :default => false

		add_column :receivers, :opened_time, :datetime
		add_column :receivers, :read_time, :datetime
		add_column :receivers, :replied_time, :datetime
		add_column :receivers, :completed_time, :datetime
  end
end
